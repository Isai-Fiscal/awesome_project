# Welcome to Awesome Project


## Features

- An awesome landing page                                                                          
- An invitation for download the app                                                               
- A contact form

## Tech Stack

- Lisp                                                                                             
- Smalltalk                                                                                        
- Amber                                                                                            
- Elm 

## Collaborators

- Isaí Niño Fiscal